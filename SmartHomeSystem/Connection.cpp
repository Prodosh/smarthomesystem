#include "Connection.h"

Connection::Connection(owner parent, asio::io_context& context, asio::ip::tcp::socket socket, MsgQueue<OwnedMessage>& msgIn)
	: _context(context), _socket(std::move(socket)), _messagesIn(msgIn), _ownerType(parent)
{

}

void Connection::connectToServer(const asio::ip::tcp::resolver::results_type& endpoint)
{
	if (_ownerType == owner::client)
	{
		asio::async_connect(_socket, endpoint,
			[this](std::error_code ec, asio::ip::tcp::endpoint endpoint) {

				if (!ec)
				{
					readHeader();
				}
				else
				{
					std::cout << "Error connecting to server: " << ec.message() << std::endl;

				}

			});
	}


}

bool Connection::disconnect()
{
	if (isConnected())
	{
		asio::post(_context, [this]() { _socket.close();  });
		return true;
	}
	return false;
}

bool Connection::isConnected() const
{
	return _socket.is_open();
}

void Connection::send(const Message& msg)
{
	asio::post(_context,
		[this, msg]() {

			bool writingMessage = !_messagesOut.empty();

			_messagesOut.push_back(msg);

			if (!writingMessage)
			{
				writeHeader();
			}
		});


}

void Connection::readHeader()
{
	asio::async_read(_socket, asio::buffer(&_msgTemporaryIn.header, sizeof(MsgHeader)),
		[this](std::error_code ec, std::size_t length) {
			if (!ec)
			{
				if (_msgTemporaryIn.header.size > 0)
				{
					_msgTemporaryIn.msgBody.resize(_msgTemporaryIn.header.size);
					readBody();
				}
				else
				{
					addToIncomingMessageQueue();
				}
			}
			else
			{
				std::cout << "Read header failed: " << ec.message() << std::endl;
				_socket.close();
			}
		}
	);
}

void Connection::readBody()
{
	asio::async_read(_socket, asio::buffer(_msgTemporaryIn.msgBody.data(), _msgTemporaryIn.msgBody.size()),
		[this](std::error_code ec, std::size_t length) {
			if (!ec)
			{
				addToIncomingMessageQueue();
			}
			else
			{
				std::cout << "Read body failed: " << ec.message() << std::endl;
				_socket.close();
			}
		}
	);
}

void Connection::addToIncomingMessageQueue()
{
	if (_ownerType == owner::server)
	{
		_messagesIn.push_back({ this->shared_from_this(), _msgTemporaryIn });

	}
	else
	{
		_messagesIn.push_back({ nullptr, _msgTemporaryIn });

	}

	readHeader();
}

void Connection::writeHeader()
{
	asio::async_write(_socket, asio::buffer(&_messagesOut.front().header, sizeof(MsgHeader)),
		[this](std::error_code ec, std::size_t length)
		{
			if (!ec)
			{
				if (_messagesOut.front().msgBody.size() > 0)
				{
					writeBody();
				}
				else
				{
					_messagesOut.pop_front();

					if (!_messagesOut.empty())
						writeHeader();
				}
			}
			else
			{
				std::cout << "Write header failed" << ec.message() << std::endl;
				_socket.close();
			}
		}
	);
}

void Connection::writeBody()
{
	asio::async_write(_socket, asio::buffer(_messagesOut.front().msgBody.data(), _messagesOut.front().msgBody.size()),
		[this](std::error_code ec, std::size_t length) {

			if (!ec)
			{
				_messagesOut.pop_front();
				if (!_messagesOut.empty())
				{
					writeHeader();
				}

			}
			else
			{
				std::cout << "Write body failed" << ec.message() << std::endl;
				_socket.close();
			}


		}
	);
}

void Connection::connectToClient(uint32_t uid)
{
	if (_ownerType == owner::server)
	{
		if (_socket.is_open())
		{
			_id = uid;
			readHeader();
		}
	}
}

uint32_t Connection::getId()
{
	return _id;
}
