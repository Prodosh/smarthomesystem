## SMART HOME SYSTEM

This program allows you to conigure sensors that report on the state of certain devices to a central controller. So far only the controller, light bulb devices 
and thermostat can be set up. They communicate to each other via HTTP. The controller is the TCP server which I have implemented with the ASIO
library while the different devices (in this version 2 devices) will be the client.

To test this program launch one instance and configure a controller device. Then open another instance of the program and configure a light bulb or thermostat.
Ensure that you enter the same port number for both of the devices. The light bulb and thermostat will report to the controller at 3 second intervals and the
controller can remotely control the light bulb via HTTP. In the future everything from reporting intervals to device names can be edited.

Currently the server and client run on localhost but functionality will be added later to run on any IP address to allow for connections between
different computers.