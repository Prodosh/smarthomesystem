#include "Client.h"

Client::Client()
{
}

Client::~Client()
{
	disconnect();
}

bool Client::connect(const std::string& host, const uint16_t port)
{
	try
	{

		asio::ip::tcp::resolver resolver(_context);
		asio::ip::tcp::resolver::results_type endpoints = resolver.resolve(host, std::to_string(port));

		_connection = std::make_unique<Connection>(Connection::owner::client, _context, asio::ip::tcp::socket(_context), _messagesIn);

		_connection->connectToServer(endpoints);


		_contextThread = std::thread([this]() { _context.run();  });

	}
	catch (std::exception& e)
	{
		std::cerr << "Client exception: " << e.what() << std::endl;
		return false;
	}

	return true;
}

void Client::disconnect()
{
	if (isConnected())
	{
		_connection->disconnect();
	}

	_context.stop();

	if (_contextThread.joinable())
		_contextThread.join();

	_connection.release();
}

void Client::send(const Message& msg)
{
	if (isConnected())
	{
		_connection->send(msg);
	}
}

bool Client::isConnected()
{
	if (_connection)
		return _connection->isConnected();
	else
		return false;
}
