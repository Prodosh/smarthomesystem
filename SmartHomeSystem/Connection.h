#pragma once

#ifndef CONNECTION_H
#define CONNECTION_H

#include "Common.h"
#include "MsgQueue.h"
#include "Message.h"

class Connection : public std::enable_shared_from_this<Connection>
{
public:
	enum class owner
	{
		server,
		client
	};

	Connection(owner parent, asio::io_context& context, asio::ip::tcp::socket socket, MsgQueue<OwnedMessage>& msgIn);


	~Connection() {}

public:

	void connectToServer(const asio::ip::tcp::resolver::results_type& endpoint);


	bool disconnect();


	bool isConnected() const;


	void send(const Message& msg);



	void readHeader();


	void readBody();


	void addToIncomingMessageQueue();


	void writeHeader();


	void writeBody();


	void connectToClient(uint32_t uid = 0);




	uint32_t getId();



protected:

	asio::ip::tcp::socket _socket;

	asio::io_context& _context;

	MsgQueue<Message> _messagesOut;

	MsgQueue<OwnedMessage>& _messagesIn;

	Message _msgTemporaryIn;

	owner _ownerType = owner::server;
	
	uint32_t _id = 0;

};


#endif