#include "LightSwitchProxy.h"

LightSwitchProxy::LightSwitchProxy(std::shared_ptr<Connection> lightSwitchClient, Server* controllerServer)
	: AbstractLightSwitch(DeviceType::LIGHT_BULB), _lightSwitchClient(lightSwitchClient), _controllerServer(controllerServer)
{

}



void LightSwitchProxy::currentState()
{
	Message status;
	status.header.requestType = RequestType::GET;
	status.header.deviceType = DeviceType::CONTROLLER;
	status.header.msgType = MessageType::STATUS;

	_controllerServer->messageClient(_lightSwitchClient, status);
}



void LightSwitchProxy::turnOn()
{
	Message onMsg;
	onMsg.header.requestType = RequestType::PUT;
	onMsg.header.deviceType = DeviceType::CONTROLLER;
	onMsg.header.msgType = MessageType::TURN_ON;

	_controllerServer->messageClient(_lightSwitchClient, onMsg);
}

void LightSwitchProxy::turnOff()
{
	Message offMsg;
	offMsg.header.requestType = RequestType::PUT;
	offMsg.header.deviceType = DeviceType::CONTROLLER;
	offMsg.header.msgType = MessageType::TURN_OFF;

	_controllerServer->messageClient(_lightSwitchClient, offMsg);
}

void LightSwitchProxy::dim()
{
	Message dimMsg;
	dimMsg.header.requestType = RequestType::PUT;
	dimMsg.header.deviceType = DeviceType::CONTROLLER;
	dimMsg.header.msgType = MessageType::DIM;

	_controllerServer->messageClient(_lightSwitchClient, dimMsg);
}

void LightSwitchProxy::brighten()
{
	Message brightenMsg;
	brightenMsg.header.requestType = RequestType::PUT;
	brightenMsg.header.deviceType = DeviceType::CONTROLLER;
	brightenMsg.header.msgType = MessageType::BRIGHTEN;

	_controllerServer->messageClient(_lightSwitchClient, brightenMsg);
}
