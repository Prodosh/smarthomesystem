#pragma once
#ifndef MESSAGE_ENUMS_H
#define MESSAGE_ENUMS_H

enum class RequestType {
	GET,
	POST,
	PUT,
	DEL
};


enum class MessageType {
	STATUS,
	BRIGHTEN,
	DIM,
	TURN_ON,
	TURN_OFF,
	LAST_MEASUREMENT,
	SET_POINT,
	WARMER,
	COOLER
};

enum class DeviceType {
	CONTROLLER,
	LIGHT_BULB,
	THERMOSTAT,
	TOTAL_DEVICES
};


#endif
