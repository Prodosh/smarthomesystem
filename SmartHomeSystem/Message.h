#pragma once

#ifndef MESSAGE_H
#define MESSAGE_H

#include "Common.h"
#include "MessageEnums.h"

struct MsgHeader {
	RequestType requestType{};
	DeviceType deviceType{};
	MessageType msgType{};

	uint32_t size = 0;
};


struct Message {
	MsgHeader header;

	std::vector<uint8_t> msgBody;

	size_t getSize() const
	{
		return msgBody.size();
	}


	friend std::ostream& operator<< (std::ostream& out, const Message& msg)
	{
		out << "Request Type: " << int(msg.header.requestType) << " Message Type: " << int(msg.header.msgType) << " Device type: " << int(msg.header.deviceType) << " Size: " << msg.header.size;
		return out;
	}

	template<typename D>
	friend Message& operator << (Message& msg, const D& data)
	{
		size_t currentSize = msg.msgBody.size();

		msg.msgBody.resize(currentSize + sizeof(D));

		std::memcpy(msg.msgBody.data() + currentSize, &data, sizeof(D));

		msg.header.size = msg.getSize();

		return msg;
	}


	template<typename D>
	friend Message& operator >> (Message& msg, D& data)
	{
		size_t sizePostExtraction = msg.msgBody.size() - sizeof(D);

		std::memcpy(&data, msg.msgBody.data() + sizePostExtraction, sizeof(D));

		msg.msgBody.resize(sizePostExtraction);

		msg.header.size = msg.getSize();

		return msg;
	}



};

class Connection;

struct OwnedMessage
{
	std::shared_ptr<Connection> remote = nullptr;

	Message msg;

	friend std::ostream& operator<< (std::ostream& out, const OwnedMessage& msg)
	{
		out << msg.msg;
		return out;
	}
};

#endif

