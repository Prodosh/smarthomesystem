#include "LightSwitch.h"

LightSwitch::LightSwitch(const std::string& controllerAddress, const uint16_t controllerPort)
	: AbstractLightSwitch(DeviceType::LIGHT_BULB)
{
	_client.connect(controllerAddress, controllerPort);
}

void LightSwitch::run()
{
	using namespace std::chrono;
	uint64_t seconds = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

	while (true)
	{
		if (_client.isConnected())
		{
			if (!_client._messagesIn.empty())
			{
				Message msg = _client._messagesIn.pop_front().msg;

				if (msg.header.deviceType == DeviceType::CONTROLLER)
				{
					if (msg.header.requestType == RequestType::PUT && msg.header.msgType == MessageType::TURN_ON)
						turnOn();
					else if (msg.header.requestType == RequestType::PUT && msg.header.msgType == MessageType::TURN_OFF)
						turnOff();
					else if (msg.header.requestType == RequestType::PUT && msg.header.msgType == MessageType::BRIGHTEN)
						brighten();
					else if (msg.header.requestType == RequestType::PUT && msg.header.msgType == MessageType::DIM)
						dim();




					sendStatus();
				}
			}
		}

		if (duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count() - seconds >= _interval)
		{
			currentState();

			if (_client.isConnected())
				sendStatus();

			seconds = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
		}
	}
}

void LightSwitch::currentState()
{
	std::cout << "\n\nLight is: " << (_isOn ? "ON" : "OFF") << " \n";
	std::cout << "Brightness: " << _brightness << "%" << std::endl;
}

void LightSwitch::turnOn()
{
	_isOn = true;

}

void LightSwitch::turnOff()
{
	_isOn = false;


}

void LightSwitch::dim()
{
	if (_brightness > 10)
		_brightness -= 10;
}

void LightSwitch::brighten()
{
	{
		if (_brightness < 100)
			_brightness += 10;
	}
}

void LightSwitch::sendStatus()
{
	Message status;
	status.header.requestType = RequestType::PUT;
	status.header.deviceType = DeviceType::LIGHT_BULB;
	status.header.msgType = MessageType::STATUS;

	status << _isOn;
	status << _brightness;


	_client.send(status);
	//std::cout << "Here" << std::endl;
}
