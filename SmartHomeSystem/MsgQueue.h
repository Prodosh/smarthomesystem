#pragma once
#ifndef MSG_QUEUE_H
#define MSG_QUEUE_H

#include "Message.h"
#include "Common.h"

template<typename T>
class MsgQueue
{
public:

	MsgQueue() = default;
	MsgQueue(const MsgQueue&) = delete;

	~MsgQueue() { clear(); }

	const T& front()
	{
		std::scoped_lock lock(muxQueue);
		return msgQueue.front();
	}

	const T& back()
	{
		std::scoped_lock lock(muxQueue);
		return msgQueue.back();
	}

	void push_back(const T& msg)
	{
		std::scoped_lock lock(muxQueue);
		msgQueue.emplace_back(std::move(msg));

		std::unique_lock<std::mutex> uniqueLock(muxBlocking);
		cvBlocking.notify_one();
	}

	void push_front(const T& msg)
	{
		std::scoped_lock lock(muxQueue);
		msgQueue.emplace_front(std::move(msg));

		std::unique_lock<std::mutex> uniqueLock(muxBlocking);
		cvBlocking.notify_one();
	}

	T pop_front()
	{
		std::scoped_lock lock(muxQueue);

		T msg = std::move(msgQueue.front());
		msgQueue.pop_front();

		return msg;
	}

	T pop_back()
	{
		std::scoped_lock lock(muxQueue);

		T msg = std::move(msgQueue.back());
		msgQueue.pop_back();

		return msg;
	}

	bool empty()
	{
		std::scoped_lock lock(muxQueue);
		return msgQueue.empty();
	}

	size_t count()
	{
		std::scoped_lock lock(muxQueue);
		return msgQueue.size();
	}

	void clear()
	{
		std::scoped_lock lock(muxQueue);
		msgQueue.clear();
	}

	void wait()
	{
		while (empty())
		{
			std::unique_lock<std::mutex> uniqueLock(muxBlocking);
			cvBlocking.wait(uniqueLock);
		}
	}

protected:
	std::mutex muxQueue;
	std::deque<T> msgQueue;

	std::condition_variable cvBlocking;
	std::mutex muxBlocking;
};

#endif