#pragma once
#ifndef THERMOSTAT_PROXY_H
#define THERMOSTAT_PROXY_H


#include "AbstractThermostat.h"

#include "Server.h"
#include "Connection.h";

class ThermostatProxy :
	public AbstractThermostat
{
public:
	ThermostatProxy(std::shared_ptr<Connection> lightSwitchClient, Server* controllerServer);

	void lastMeasurement() override;


	void setPoint() override;

	void currentState() override;

	void warmer(int amount) override;

	void cooler(int amount) override;

private:

	std::shared_ptr<Connection> _thermostatClient;

	Server* _controllerServer;

};


#endif THERMOSTAT_PROXY_H

