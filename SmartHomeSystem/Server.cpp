#include "Server.h"

#include "SmartController.h"

Server::Server(uint16_t port, SmartController& smartController)
	: _asioAcceptor(_context, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port)), _smartController(smartController)
{

}

Server::~Server()
{
	stop();
}

bool Server::start()
{
	try
	{
		waitForClientConnection();

		_threadContext = std::thread([this]() { _context.run();  });

	}
	catch (std::exception& e)
	{
		std::cerr << "Server exception: " << e.what() << std::endl;
		return false;
	}

	std::cout << "[SERVER] Started!\n";
	return true;
}

void Server::stop()
{
	_context.stop();

	if (_threadContext.joinable())
		_threadContext.join();

	std::cout << "[SERVER] stopped!\n";

}

void Server::waitForClientConnection()
{
	_asioAcceptor.async_accept(
		[this](std::error_code ec, asio::ip::tcp::socket socket)
		{
			if (!ec)
			{
				std::cout << "New connection: " << socket.remote_endpoint() << std::endl;

				std::shared_ptr<Connection> conn = std::make_shared<Connection>(Connection::owner::server, _context, std::move(socket), _messagesIn);

				if (onClientConnect(conn))
				{
					_connections.push_back(std::move(conn));

					_connections.back()->connectToClient(clientID++);

					std::cout << "Connection Successful!" << std::endl;
				}
				else
				{
					std::cout << "Connection denied" << std::endl;
				}
			}
			else
			{
				std::cout << "Server error when making new connection: " << ec.message() << std::endl;
			}

			waitForClientConnection();
		}


	);
}

void Server::messageClient(std::shared_ptr<Connection> client, const Message& msg)
{
	if (client && client->isConnected())
	{
		client->send(msg);
	}
	else
	{
		onClientDisconnect(client);
		client.reset();

		_connections.erase(std::remove(_connections.begin(), _connections.end(), client), _connections.end());

	}
}

void Server::messageAllClients(const Message& msg, std::shared_ptr<Connection> ignore)
{
	bool invalidClientExists = false;
	for (auto& client : _connections)
	{
		if (client && client->isConnected())
		{
			if (client != ignore)
				client->send(msg);
		}
		else
		{
			onClientDisconnect(client);
			client.reset();

			invalidClientExists = true;

		}
	}

	if (invalidClientExists)
	{
		_connections.erase(std::remove(_connections.begin(), _connections.end(), nullptr), _connections.end());
	}
}

void Server::update(size_t maxMessages, bool wait)
{
	if (wait)
		_messagesIn.wait();

	size_t messageCount = 0;
	while (messageCount < maxMessages && !_messagesIn.empty())
	{
		auto msg = _messagesIn.pop_front();

		onMessage(msg.remote, msg.msg);

		++messageCount;
	}
}

bool Server::onClientConnect(std::shared_ptr<Connection> client)
{
	return true;
}

void Server::onClientDisconnect(std::shared_ptr<Connection> client)
{
	_smartController.deviceDisconnected(client);
}

void Server::onMessage(std::shared_ptr<Connection> client, Message& msg)
{
	_smartController.onMessage(client, msg);
}

