#pragma once

#ifndef ABSTRACT_THERMOSTAT_H
#define ABSTRACT_THERMOSTAT_H

#include "Device.h"
#include <iostream>

class AbstractThermostat : public Device
{
public:
	AbstractThermostat()
		:Device(DeviceType::THERMOSTAT)
	{

	}


	void menu()
	{
		while (true)
		{
			std::cout << "Thermostat menu: Enter number next to command to execute" << std::endl;
			std::cout << "1. Read last measurement" << std::endl;
			std::cout << "2. Current temperature set point" << std::endl;
			std::cout << "3. Current State" << std::endl;
			std::cout << "4. Warmer" << std::endl;
			std::cout << "5. Cooler" << std::endl;
			std::cout << "6. Quit" << std::endl;

			int x;
			std::cin >> x;

			switch (x)
			{
			case 1:
				lastMeasurement();
				break;
			case 2:
				setPoint();
				break;
			case 3:
				currentState();
				break;
			case 4:
				{
				std::cout << "Please enter ammount to warm by: ";
				int warmerTemp;
				std::cin >> warmerTemp;
				warmer(warmerTemp);
					break;
				}				
			case 5:
				{
				std::cout << "Please enter ammount to cool by: ";
				int coolerTemp;
				std::cin >> coolerTemp;
				cooler(coolerTemp);
				break;
				}
			case 6:
				return;
			default:
				std::cout << "Invalid command please try again" << std::endl;

			}
		}

	}

	virtual void lastMeasurement() = 0;


	virtual void setPoint() = 0;

	virtual void currentState() = 0;

	virtual void warmer(int amount) = 0;

	virtual void cooler(int amount) = 0;
};



#endif
