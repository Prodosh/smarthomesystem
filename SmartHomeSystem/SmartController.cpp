#include "SmartController.h"
#include "Thermostat.h"
//#include "Server.h"

SmartController::SmartController(uint16_t port)
	: Device(DeviceType::CONTROLLER)
{
	_controllerServer = std::make_unique<Server>(port, *this);
}

void SmartController::onMessage(std::shared_ptr<Connection> client, Message& msg)
{
	if (_connectionMap.find(client->getId()) == _connectionMap.end())
	{
		DeviceInfo dInfo(msg.header.deviceType, client);

		std::cout << "Connected " << dInfo.getName() << std::endl;

		_connectionMap[client->getId()] = dInfo;
	}
	else
	{
		if (msg.header.deviceType == DeviceType::LIGHT_BULB)
		{
			if (msg.header.msgType == MessageType::STATUS && msg.header.requestType == RequestType::PUT)
			{
				int brightness;
				bool isOn;

				msg >> brightness;
				msg >> isOn;


				std::cout << "\n\nLight is: " << (isOn ? "ON" : "OFF") << " \n";
				std::cout << "Brightness: " << brightness << "%" << std::endl;
			}
		}
		else if (msg.header.deviceType == DeviceType::THERMOSTAT)
		{
			if (msg.header.msgType == MessageType::STATUS && msg.header.requestType == RequestType::PUT)
			{
				Thermostat::State state;
				float lastMeasurement = 0;
				float setPoint = 0;
				char tempMetric = 'C';

				msg >> state;
				msg >> setPoint;
				msg >> tempMetric;
				msg >> lastMeasurement;

				std::string st;

				switch (state)
				{
				case Thermostat::State::COOLING:
					st = "COOLING";
					break;
				case Thermostat::State::HEATING:
					st = "HEATING";
					break;
				case Thermostat::State::STABLE:
					st = "STABLE";
					break;
				default:
					st = "INVALID STATE";

				}


				std::cout << "\n\nSTATE: " << st << std::endl;
				std::cout << "Last measurement: " << lastMeasurement << tempMetric << std::endl;
				std::cout << "SET POINT: " << setPoint << tempMetric << std::endl;
			}
			else if(msg.header.msgType == MessageType::LAST_MEASUREMENT && msg.header.requestType == RequestType::PUT)
			{
				float lastMeasurement = 0;
				char tempMetric = 'C';

				msg >> tempMetric;
				msg >> lastMeasurement;

				std::cout << "Last measurement: " << lastMeasurement << tempMetric << std::endl;
			}
			else if (msg.header.msgType == MessageType::SET_POINT && msg.header.requestType == RequestType::PUT)
			{
				float setpoint = 0;
				char tempMetric = 'C';

				msg >> tempMetric;
				msg >> setpoint;

				std::cout << "SET POINT: " << setpoint << tempMetric << std::endl;
			}
		}
	}

}

void SmartController::startServer()
{
	_controllerServer->start();

	while (true)
	{
		_controllerServer->update(-1, true);
	}
}

void SmartController::menu()
{
	while (true)
	{
		std::cout << "Smart controller menu: Enter number next to command to execute" << std::endl;
		std::cout << "1. See Available Devices" << std::endl;

		int x;
		std::cin >> x;

		switch (x)
		{
		case 1:
			deviceMenu();
			break;
		case 5:
			return;
		default:
			std::cout << "Invalid command please try again" << std::endl;

		}
	}

}

void SmartController::deviceMenu()
{
	std::cout << "Here are your available devices: Please enter in ID of device you would like to control: " << std::endl;

	while (true)
	{
		for (int num = 0; num < _controllerServer->_connections.size(); ++num)
		{
			if (_connectionMap.find(_controllerServer->_connections[num]->getId()) != _connectionMap.end())
			{
				uint32_t id = _controllerServer->_connections[num]->getId();
				std::cout << num + 1 << ". " << _connectionMap[id].getName() << " ID: " << id << std::endl;
			}
		}
		std::cout << "Enter 1 to go back" << std::endl;

		int command;
		std::cin >> command;

		if (command == 1)
			return;
		else
		{
			if (_connectionMap.find(command) != _connectionMap.end())
			{
				DeviceInfo& dev = _connectionMap[command];

				if (dev.getName() == "LIGHT_SWITCH")
				{
					LightSwitchProxy lightProxy(dev.getClientDevice(), _controllerServer.get());
					lightProxy.menu();
				}
				else if (dev.getName() == "THERMOSTAT")
				{
					ThermostatProxy thermoProxy(dev.getClientDevice(), _controllerServer.get());
					thermoProxy.menu();
				}
			}
		}

	}
}

void SmartController::deviceDisconnected(std::shared_ptr<Connection> client)
{
	std::cout << "Divice has been disconnected please exit the menu" << std::endl;

	if (_connectionMap.find(client->getId()) != _connectionMap.end())
	{
		_connectionMap.erase(client->getId());
	}
}
