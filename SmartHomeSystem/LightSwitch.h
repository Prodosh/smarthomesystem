#pragma once
#ifndef LIGHT_SWITCH
#define LIGHT_SWITCH

#include "AbstractLightSwitch.h"
#include "Client.h"
#include <iostream>


class LightSwitch : public AbstractLightSwitch
{
public:

	LightSwitch(const std::string& controllerAddress, const uint16_t controllerPort);

	void run();

	void currentState() override;

	void turnOn() override;

	void turnOff() override;

	void dim() override;

	void brighten() override;


private:

	void sendStatus();



private:

	int _brightness = 50;
	bool _isOn = false;

	uint64_t _interval = 3000;

	Client _client;
};



#endif // !LIGHT_SWITCH
