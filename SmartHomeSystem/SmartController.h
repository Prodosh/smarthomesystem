#pragma once
#ifndef SMART__CONTROLLER
#define SMART__CONTROLLER

#include "Device.h"
#include "MessageEnums.h"
#include "Connection.h"
#include "DeviceInfo.h"
#include <unordered_map>
#include <string>

#include "Server.h"
#include "LightSwitchProxy.h"
#include "ThermostatProxy.h"

class SmartController : public Device
{
public:

	SmartController(uint16_t port);

	void onMessage(std::shared_ptr<Connection> client, Message& msg);

	void startServer();

	void menu();

	void deviceMenu();

	void deviceDisconnected(std::shared_ptr<Connection> client);


private:

	std::unordered_map<uint32_t, DeviceInfo> _connectionMap;


	std::unique_ptr<Server> _controllerServer;

};


#endif