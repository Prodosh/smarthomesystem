#pragma once
#ifndef DEVICE_INFO_H
#define DEVICE_INFO_H

#include "Device.h"
#include "Connection.h"
#include <string>
class DeviceInfo :
	public Device
{
public:
	DeviceInfo();

	DeviceInfo(DeviceType deviceType, std::shared_ptr<Connection> clientDevice);


	std::string& getName();

	uint32_t getID();

	std::shared_ptr<Connection> getClientDevice();

private:

	std::string _name;

	std::shared_ptr<Connection> _clientDevice;

	
	uint32_t _id;
};

#endif

