#include "Thermostat.h"

Thermostat::Thermostat(float setPoint, char tempMetric, const std::string& controllerAddress, const uint16_t controllerPort)
	: _setPoint(setPoint), _tempMetric(tempMetric)
{
	_client.connect(controllerAddress, controllerPort);
}

void Thermostat::run()
{
	using namespace std::chrono;
	uint64_t seconds = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

	while (true)
	{
		if (_client.isConnected())
		{
			if (!_client._messagesIn.empty())
			{
				Message msg = _client._messagesIn.pop_front().msg;

				if (msg.header.deviceType == DeviceType::CONTROLLER)
				{
					if (msg.header.requestType == RequestType::GET && msg.header.msgType == MessageType::STATUS)
						sendStatus();
					else if (msg.header.requestType == RequestType::GET && msg.header.msgType == MessageType::LAST_MEASUREMENT)
						sendLastMeasurement();
					else if (msg.header.requestType == RequestType::GET && msg.header.msgType == MessageType::SET_POINT)
						sendSetPoint();
					else if (msg.header.requestType == RequestType::POST && msg.header.msgType == MessageType::WARMER)
					{
						int warmAmount;
						msg >> warmAmount;

						warmer(warmAmount);
					}
					else if (msg.header.requestType == RequestType::POST && msg.header.msgType == MessageType::COOLER)
					{
						int coolAmount;
						msg >> coolAmount;

						warmer(coolAmount);
					}

				}
			}
		}

		if (duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count() - seconds >= _interval)
		{
			std::cout << "\n\n";
			lastMeasurement();
			setPoint();
			currentState();

			//float min, max = 0f;
			if (_state == State::HEATING)
			{
				_lastMeasurement += 0.5 > (_setPoint - _lastMeasurement) / 10 ? 0.5 : (_setPoint - _lastMeasurement) / 10;

			}
			else if (_state == State::COOLING)
			{
				_lastMeasurement -= 0.5 > (_lastMeasurement - _setPoint) / 10 ? 0.5 : (_lastMeasurement - _setPoint) / 10;
			}
			else
			{
				std::random_device rd;
				std::default_random_engine eng(rd());
				std::uniform_real_distribution<float> distr(_setPoint - 0.5, _setPoint + 0.5);

				_lastMeasurement = distr(eng);
			}



			if (_client.isConnected())
				sendStatus();

			seconds = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
		}
	}
}

void Thermostat::lastMeasurement()
{
	std::cout << "Last measurement: " << _lastMeasurement << _tempMetric << std::endl;
}

void Thermostat::setPoint()
{
	std::cout << "SET POINT: " << _setPoint << _tempMetric << std::endl;
}

void Thermostat::currentState()
{
	if (_currentTemp < _setPoint - 0.5)
		_state = State::HEATING;
	else if (_currentTemp > _setPoint + 0.5)
		_state = State::COOLING;
	else
		_state = State::STABLE;

	std::string st;

	switch (_state)
	{
	case State::COOLING:
		st = "COOLING";
		break;
	case State::HEATING:
		st = "HEATING";
		break;
	case State::STABLE:
		st = "STABLE";
		break;
	default:
		st = "INVALID STATE";

	}


	std::cout << "STATE: " << st << std::endl;
}

void Thermostat::warmer(int amount)
{
	if (amount > 0)
		_setPoint += amount;
}

void Thermostat::cooler(int amount)
{
	if (amount > 0)
		_setPoint -= amount;
}

void Thermostat::sendStatus()
{
	Message  msg;

	msg.header.deviceType = DeviceType::THERMOSTAT;
	msg.header.msgType = MessageType::STATUS;
	msg.header.requestType = RequestType::PUT;

	msg << _lastMeasurement;
	msg << _tempMetric;
	msg << _setPoint;
	msg << _state;

	_client.send(msg);
}

void Thermostat::sendLastMeasurement()
{
	Message  msg;

	msg.header.deviceType = DeviceType::THERMOSTAT;
	msg.header.msgType = MessageType::LAST_MEASUREMENT;
	msg.header.requestType = RequestType::PUT;

	msg << _lastMeasurement;
	msg << _tempMetric;

	_client.send(msg);
}

void Thermostat::sendSetPoint()
{
	Message  msg;

	msg.header.deviceType = DeviceType::THERMOSTAT;
	msg.header.msgType = MessageType::SET_POINT;
	msg.header.requestType = RequestType::PUT;

	msg << _setPoint;
	msg << _tempMetric;

	_client.send(msg);
}
