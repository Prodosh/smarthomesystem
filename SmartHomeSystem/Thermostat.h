#pragma once
#ifndef THERMOSTAT_H
#define THERMOSTAT_H

#include <string>
#include <random>
#include "AbstractThermostat.h"
#include "Client.h"


class Thermostat : public AbstractThermostat
{
public:

	enum class State
	{
		HEATING,
		COOLING,
		STABLE
	};



	Thermostat(float setPoint, char tempMetric, const std::string& controllerAddress, const uint16_t controllerPort);



	void run();


	virtual void lastMeasurement() override;

	virtual void setPoint() override;

	virtual void currentState() override;

	virtual void warmer(int amount) override;

	virtual void cooler(int amount) override;


private:
	void sendStatus();


	void sendLastMeasurement();


	void sendSetPoint();



private:
	float _currentTemp = 10;
	float _lastMeasurement = _currentTemp;
	float _setPoint = 20;
	char _tempMetric = 'C';

	State _state;

	Client _client;

	uint64_t _interval = 3000;
};


#endif // !THERMOSTAT_H
