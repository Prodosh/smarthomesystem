#pragma once
#ifndef CLIENT_H
#define CLIENT_H

#include "Common.h"
#include "MsgQueue.h"
#include "Message.h"
#include "Connection.h"

class Client
{
public:

	Client();


	~Client();


	bool connect(const std::string& host, const uint16_t port);


	void disconnect();


	void send(const Message& msg);


	bool isConnected();


	MsgQueue<OwnedMessage> _messagesIn;

private:
	

	asio::io_context _context;

	std::thread _contextThread;

	//asio::ip::tcp::socket _socket;

	std::unique_ptr<Connection> _connection;
};

#endif