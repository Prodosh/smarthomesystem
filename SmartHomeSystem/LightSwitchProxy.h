#pragma once
#include "AbstractLightSwitch.h"

#include "Server.h"
#include "Connection.h";

class LightSwitchProxy :
	public AbstractLightSwitch
{

public:

	LightSwitchProxy(std::shared_ptr<Connection> lightSwitchClient, Server* controllerServer);



	void currentState() override;

	void turnOn() override;

	void turnOff() override;

	void dim() override;

	void brighten() override;

private:

	std::shared_ptr<Connection> _lightSwitchClient;

	Server* _controllerServer;
};

