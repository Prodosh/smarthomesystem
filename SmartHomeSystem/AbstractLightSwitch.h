#pragma once
#ifndef ABSTRACT_LIGHT_SWITCH_H
#define ABSTRACT_LIGHT_SWITCH_H

#include "Device.h"
#include <iostream>

class AbstractLightSwitch : public Device
{
public:
	AbstractLightSwitch(DeviceType deviceType)
		: Device(deviceType)
	{

	}

	void menu()
	{
		while (true)
		{
			std::cout << "Light switch menu: Enter number next to command to execute" << std::endl;
			std::cout << "1. Turn On" << std::endl;
			std::cout << "2. Turn Off" << std::endl;
			std::cout << "3. Brighten" << std::endl;
			std::cout << "4. Dim" << std::endl;
			std::cout << "5. Quit" << std::endl;

			int x;
			std::cin >> x;

			switch (x)
			{
			case 1:
				turnOn();
				break;
			case 2:
				turnOff();
				break;
			case 3:
				brighten();
				break;
			case 4:
				dim();
				break;
			case 5:
				return;
			default:
				std::cout << "Invalid command please try again" << std::endl;

			}
		}
	}

	virtual void currentState() = 0;

	virtual void turnOn() = 0;

	virtual void turnOff() = 0;

	virtual void dim() = 0;

	virtual void brighten() = 0;
};

#endif