#pragma once
#ifndef DEVICE_H
#define DEVICE_H

#include "MessageEnums.h"
#include <string>


class Device
{
public:
	Device()
	{
		_deviceType = DeviceType::CONTROLLER;
	}


	Device(DeviceType deviceType)
		: _deviceType(deviceType)
	{

	}




protected:
	DeviceType _deviceType;


};

#endif // !DEVICE_H
