#include "ThermostatProxy.h"

ThermostatProxy::ThermostatProxy(std::shared_ptr<Connection> thermostatClient, Server* controllerServer)
	: AbstractThermostat(), _thermostatClient(thermostatClient), _controllerServer(controllerServer)
{
}

void ThermostatProxy::lastMeasurement()
{
	Message lastMeasurement;
	lastMeasurement.header.requestType = RequestType::GET;
	lastMeasurement.header.msgType = MessageType::STATUS;
	lastMeasurement.header.msgType = MessageType::LAST_MEASUREMENT;

	_controllerServer->messageClient(_thermostatClient, lastMeasurement);
}

void ThermostatProxy::setPoint()
{
	Message setpoint;
	setpoint.header.requestType = RequestType::GET;
	setpoint.header.deviceType = DeviceType::CONTROLLER;
	setpoint.header.msgType = MessageType::SET_POINT;

	_controllerServer->messageClient(_thermostatClient, setpoint);
}

void ThermostatProxy::currentState()
{
	Message status;
	status.header.requestType = RequestType::GET;
	status.header.deviceType = DeviceType::CONTROLLER;
	status.header.msgType = MessageType::STATUS;

	_controllerServer->messageClient(_thermostatClient, status);
}

void ThermostatProxy::warmer(int amount)
{
	Message warmer;
	warmer.header.requestType = RequestType::POST;
	warmer.header.deviceType = DeviceType::CONTROLLER;
	warmer.header.msgType = MessageType::WARMER;

	warmer << amount;

	_controllerServer->messageClient(_thermostatClient, warmer);
}

void ThermostatProxy::cooler(int amount)
{
	Message cooler;
	cooler.header.requestType = RequestType::POST;
	cooler.header.deviceType = DeviceType::CONTROLLER;
	cooler.header.msgType = MessageType::COOLER;

	cooler << amount;

	_controllerServer->messageClient(_thermostatClient, cooler);
}
