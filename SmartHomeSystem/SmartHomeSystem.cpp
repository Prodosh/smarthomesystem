// SmartHomeSystem.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include <asio.hpp>
#include <asio/ts/buffer.hpp>
#include <asio/ts/internet.hpp>
#include <thread>

#include "LightSwitch.h"
#include "SmartController.h"
#include "Thermostat.h"

int main()
{
    std::cout << "Welcome to your smart home system\n Please select device to configure: (Enter number)" << std::endl;
    std::cout << "1. Smart Controller" << std::endl;
    std::cout << "2. Light Bulb" << std::endl;
    std::cout << "3. Thermostat" << std::endl;

    int x;

    std::cin >> x;

    if (x == 2)
    {
        //std::cout << "Please enter ip address of light switch: " << std::endl;
        //std::string address = "";
        //std::getline(std::cin, address);

        std::cout << "Please enter port number of controller: " << std::endl;
        uint16_t port;
        std::cin >> port;

        //std::cin.clear();

        LightSwitch L("127.0.0.1", port);

        std::thread menuThread(&LightSwitch::menu, &L);


        L.run();

        menuThread.join();
    }
    else if (x == 1)
    {
        std::cout << "Please enter port the controller will listen on: " << std::endl;
        uint16_t port;
        std::cin >> port;

        SmartController controller(port);

        std::thread menuThread(&SmartController::menu, &controller);




        controller.startServer();
        menuThread.join();
        
    }
    else if (x == 3)
    {
        std::cout << "Please enter port number of controller: " << std::endl;
        uint16_t port;
        std::cin >> port;

        

        std::cout << "Enter target temperature of thermostat: ";
        float setpoint;
        std::cin >> setpoint;

        std::cout << "Enter temp metric C or F: ";
        char tempMetric;
        std::cin >> tempMetric;

        Thermostat T(setpoint, tempMetric, "127.0.0.1", port);


        std::thread menuThread(&Thermostat::menu, &T);


        T.run();

        menuThread.join();

    }

    return 0;
}


