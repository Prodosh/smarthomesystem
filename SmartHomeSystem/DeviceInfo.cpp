#include "DeviceInfo.h"

DeviceInfo::DeviceInfo()
{

}

DeviceInfo::DeviceInfo(DeviceType deviceType, std::shared_ptr<Connection> clientDevice)
	: Device(deviceType), _clientDevice(clientDevice)
{

	_id = clientDevice->getId();

	switch (deviceType)
	{
	case DeviceType::LIGHT_BULB:
		_name = "LIGHT_SWITCH";
		break;
	case DeviceType::THERMOSTAT:
		_name = "THERMOSTAT";
		break;
	default:
		_name = "UNKNOWN DEVICE DETECTED";
	}

}

std::string& DeviceInfo::getName()
{
	return _name;
}

uint32_t DeviceInfo::getID()
{
	return _id;
}

std::shared_ptr<Connection> DeviceInfo::getClientDevice()
{
	return _clientDevice;
}
