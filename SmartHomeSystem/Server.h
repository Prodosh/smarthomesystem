#pragma once
#ifndef SERVER_H
#define SERVER_H

#include "Common.h"
#include "MsgQueue.h"
#include "Message.h"
#include "Connection.h"

class SmartController;

class Server
{
public:
	Server(uint16_t port, SmartController& smartController);

	~Server();



	bool start();


	void stop();

	void waitForClientConnection();

	void messageClient(std::shared_ptr<Connection> client, const Message& msg);


	void messageAllClients(const Message& msg, std::shared_ptr<Connection> ignore = nullptr);


	void update(size_t maxMessages = -1, bool wait = false);


	bool onClientConnect(std::shared_ptr<Connection> client);


	void onClientDisconnect(std::shared_ptr<Connection> client);


	void onMessage(std::shared_ptr<Connection> client, Message& msg);

	std::deque<std::shared_ptr<Connection>> _connections;
private:

	MsgQueue<OwnedMessage> _messagesIn;

	asio::io_context _context;
	std::thread _threadContext;


	asio::ip::tcp::acceptor _asioAcceptor;

	



	uint32_t clientID = 10000;

	SmartController& _smartController;
};



#endif